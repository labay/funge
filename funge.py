from requests_html import HTMLSession
import requests
from requests.exceptions import ReadTimeout
from urllib3.exceptions import MaxRetryError, ConnectionError, NewConnectionError

from time import sleep
import re
import json
from pathlib import Path
import argparse
import progressbar
import logging

class NFT:
    def __init__(self, session, collection, index=None, url=None, path='./files'):
        self.session = session
        self.collection = collection
        if index is not None:
            self.index = index
            self.url = self.collection.tokens[index]['link']
        elif url is not None:
            self.index = None
            self.url = url
        else:
            raise ValueError('Must supply either index or URL.')

        self.link = self.Link()
        self.path = '/'.join([path, self.collection.blockchain, self.collection.id])
        
    def Link(self):
        self.session.get(self.url)
        logger.debug('Finding link...')

        e = re.compile('([^\/]+)$')
        id = e.search(self.url).group(1)

        url = 'https://rarible.com/marketplace/api/v4/items/byIds'
        ids = []
        ids.append(id)

        r = self.session.post(url, json=ids)
        J = json.loads(r.text)

        medias = J[0]['properties']['mediaEntries']
        sourceurls = [m['url'] for m in medias if m['sizeType'] == 'SOURCE']
        if len(sourceurls) > 0:
            return sourceurls[0]
        else:
            logger.debug('...No link found!')
            return None
        return None
    
    def Download(self):
        def filename(l, r):
            filebase = re.compile('[^\/,\.,\:]+$').search(l).group(0)
            lcheaders = {k.lower(): r.headers[k] for k in r.headers.keys()}
            mimetype = lcheaders['content-type']
            ext = re.compile('\/([^\/,\+,\;]+)').search(mimetype).group(1)
            if ext == 'plain':
                raise FileNotFoundError
            else:
                return '.'.join([filebase, ext])
        
        if self.link:
            logger.debug('Downloading...')
            try:
                r = requests.get(self.link, stream=True, timeout=self.collection.timeout)
            except ReadTimeout:
                logger.debug('...Timed out. Skipping.')
                return False
            except Exception as e:
                logger.debug('...URL doesn\'t exist or something. Specific error is %s'%repr(e))
                return False

            lcheaders = {k.lower(): r.headers[k] for k in r.headers.keys()}

            if r.status_code == 404:
                logger.debug('...File not found. Skipping.')
                return False
            elif int(lcheaders['content-length']) < 1000:
                logger.debug('File length under 1kb. Skipping.')
                return False
            else:
                Path(self.path).mkdir(parents=True, exist_ok=True)
                logger.debug('Path is %s'%self.path)
                try:
                    self.filename = '/'.join([self.path, filename(self.url, r)])
                    logger.debug('Filename is %s'%self.filename)
                    with open(self.filename, 'wb') as fd:
                            fd.write(r.content)
                    return True
                except Exception as e:
                    logger.debug('Something has happened, specifically %e'%repr(e))
                    return False
        else:
            return False

class NFTCollection:
    def __init__(self, session, url=None, loadtokens=False, downloadpath='./files', delay=5, timeout=10):
        self.session = session
        self.url = url
        self.downloadpath = downloadpath
        self.loadtokens = loadtokens
        self.delay = delay
        self.timeout = timeout

        self.Load()
        self.id, self.blockchainid = self.ID()
        self.name, self.blockchain, self.address, self.blurb, self.items = self.Metadata()
        self.metadata = {'name': self.name, 'blockchain': self.blockchain, 'address': self.address, 'blurb': self.blurb, 'items': self.items}

        self.catalogfile = '/'.join([self.downloadpath, self.blockchain, self.id+'.json'])
        self.catalogpath = '/'.join([self.downloadpath, self.blockchain])
        self.catalog = self.JSON('load')
        
        if self.loadtokens:
            self.Tokens()
        elif 'tokens' in self.catalog.keys() and len(self.catalog['tokens']) > 0:
            self.tokens = self.catalog['tokens']
        else:
            self.tokens = None

    def Load(self, url=None):
        url = url if url else self.url
        logger.info('Loading %s'%url)
        self.session.get(url)

        return None
    
    def ID(self):
        urlbase = 'https://rarible.com/' + ('collection/' if '/collection/' in self.url else '')
        stub = self.url[len(urlbase):]
        logger.debug('Stub is %s'%stub)
        url = 'https://rarible.com/marketplace/api/v4/urls/' + stub
        r = self.session.get(url)
        if r.status_code == 404:
            if '/' in stub:
                e = re.compile('^([^\/]+)\/(.*)$')
                blockchain = e.search(stub).group(1)
                id = e.search(stub).group(2)
                blockchainid = blockchain.upper() + '-' + id
            else:
                id = stub
                blockchainid = stub
        else:
            J = json.loads(r.text)
            blockchainid = J['ref']
            e = re.compile('(\dx.*)$')
            id = e.search(J['ref']).group(1)
        logger.debug('Blockchain ID is %s'%blockchainid)
        logger.debug('ID is %s'%id)

        return id, blockchainid
    
    def pull(self, continuation=None, size=100):
        j = '{"filter":{"verifiedOnly":false,"sort":"LOW_PRICE_FIRST","nsfw":true,"orderSources":[],"hasMetaContentOnly":true}}'
        J = json.loads(j)
        J['size'] = size
        J['filter']['collections'] = [self.blockchainid]
        #J['blockchains'] = [self.blockchain.upper()]
        J['filter']['blockchains'] = ["ETHEREUM","POLYGON","BASE","RARI","ASTARZKEVM","KROMA","ZKSYNC","IMMUTABLEX"]
        if continuation:
            J['continuation'] = continuation

        url = 'https://rarible.com/marketplace/api/v4/items/search'
        r = self.session.post(url, json=J)

        finished = False
        L = json.loads(r.text)
        if len(L) == 0 and not continuation:
            logger.info('No tokens available.')
            finished = True
        elif len(L) == 0:
            finished = True

        n = 0
        while not finished and n < 5 and 'X-CONTINUATION' not in r.headers.keys():
            logger.debug('Continuation not found, retrying.')
            sleep(1)
            r = self.session.post(url, json=J)
            n += 1
        if n == 5:
            logger.info('No continuation and out of retries; assuming this is the last pull.')
            continuation = None
            finished = True
        elif not finished:
            continuation = r.headers['X-CONTINUATION']
            logger.debug('Continuation is %s'%continuation)

        L = json.loads(r.text)

        tokens = []
        for token in L:
            id = token['id']

            linkbase = 'https://rarible.com/token/'
            link = linkbase + id

            tokens.append({'link': link})

        return tokens, continuation, finished
    
    def grabtokens(self):
        self.session.get(self.url)

        collection = []
        continuation = None
        finished = False
        while not finished:
            tokens, continuation, finished = self.pull(continuation)
            collection += tokens
        return collection
    
    def Metadata(self):
        logger.info('Grabbing collection metadata')
        url = 'https://rarible.com/marketplace/api/v4/collections/byIds'
        ids = [self.blockchainid]
        r = self.session.post(url, json=ids)
        J = json.loads(r.text)

        name = J[0]['name']
        logger.debug('Name is %s'%name)
        blockchain = J[0]['blockchain']
        blockchain = blockchain[0]+blockchain[1:].lower()
        logger.debug('Blockchain is %s'%blockchain)
        if 'description' in J[0].keys():
            blurb = J[0]['description']
        else:
            blurb = ''
        logger.debug('Blurb is: %s'%blurb)

        url = 'https://rarible.com/marketplace/api/v4/statistics/collections/stats'
        r = self.session.post(url, json=ids)
        J = json.loads(r.text)
        address = J[0]['id']
        logger.debug('Address is %s'%address)
        items = J[0]['items']
        logger.debug('Number of items is %d'%items)

        if name and blockchain and address:
            return name, blockchain, address, blurb, items
        else:
            raise ValueError('Unable to populate metadata')
    
    def Tokens(self):
        self.loaded = self.Load()

        logger.info('Grabbing tokens')
        self.tokens = self.grabtokens()

        self.json = self.JSON('dump')
        #logger.info('Saving catalog to %s'%self.catalogfile)
        #with open(self.catalogfile, "w") as outfile:
        #    outfile.write(self.json)
        return None
    
    def Download(self, delay=5):
        n = 0
        while not self.tokens and n < 2:
            self.tokens = self.Tokens()
            if not self.tokens or len(self.tokens) == 0:
                logger.warning('No tokens loaded! Retrying srape.')
                n += 1
            else:
                tokens = self.tokens
        if n == 2:
            logger.error('No tokens loaded and out of retries. Exiting.')
            exit
        
        tokens = self.tokens
        bar = progressbar.ProgressBar(max_value=len(tokens)).start()
        for i, t in enumerate(tokens):
            logger.debug(t['link'])
            if 'file' not in t.keys():
                logger.debug('Downloading...')
                T = NFT(self.session, self, index=i, path=self.downloadpath)
                success = T.Download()
                if success:
                    t['file'] = T.filename
                    logger.debug('...%s'%T.filename)
                else:
                    logger.debug('...Failed!')
                sleep(delay)
            else:
                success = None

            self.tokens = tokens
            if success:
                self.json = self.JSON('dump')
                logger.debug('Updating catalog')
                with open(self.catalogfile, "w") as outfile:
                    outfile.write(self.json)
            bar.update(i)

        bar.finish()
        return None
    
    def JSON(self, operation='dump'):
        if operation == 'load':
            if Path(self.catalogfile).is_file():
                self.loadtokens = False
                with open(self.catalogfile, 'r') as catalogfile:
                    return json.loads(catalogfile.read())
            else:
                return self.metadata
        elif operation == 'dump':
            if self.tokens:
                return json.dumps({**self.metadata, **{'tokens': self.tokens}})
            else:
                return json.dumps(self.metadata)
        else:
            return None

def main(args):
    collections = args.collections
    if len(collections) == 1 and collections[0][-5:] == '.json':
        with open(collections[0], 'r') as data:
            JSON = json.loads(data.read())
            urls = [c['link'] for c in JSON]
        logger.info('Found %d collections in %s'%(len(urls), collections[0]))
    else:
        urls = collections
        logger.info('Found %d collections in arguments'%(len(urls)))

    downloadpath = args.downloadpath
    listonly = args.listonly

    for url in urls:
        sleep(args.delay)
        logger.info('Instantiating HTMLSession')
        session = HTMLSession()
        logger.info('Instantiating collection at %s'%url)
        try:
            C = NFTCollection(session, url, loadtokens=False, downloadpath=downloadpath, delay=args.delay, timeout=args.timeout)
            logger.info('Collection name is %s'%C.name)
            logger.debug('Collection address is %s'%C.address)
            if not C.tokens:
                logger.info('No tokens present; loading...')
                C.Tokens()
                
            logger.info('Saving information')
            logger.debug('Creating destination path if it doesn\'t exist')
            Path(C.catalogpath).mkdir(parents=True, exist_ok=True)
            if listonly:
                logger.info('Dumping catalog to %s'%C.catalogfile)
                with open(C.catalogfile, 'w') as catalogfile:
                        catalogfile.write(C.JSON('dump'))
            elif not listonly:
                if C.tokens:
                    logger.info('Downloading tokens')
                    try:
                        C.Download(args.delay)
                    except Exception as e:
                        logger.error('Token download failed with exception %s'%repr(e))
                        session.close()
                        continue
                else:
                    logger.warning('Empty collection. Skipping.')
                    session.close()
                    continue
                
        except Exception as e:
            logger.error('Instantiating collection failed with exception %s'%repr(e))
            session.close()
            continue

    logger.debug('Closing HTMLSession')
    session.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Right-click some NFTs from Rarible')
    parser.add_argument('collections', type=str, nargs='*', help='The Rarible collection(s) to be funged. Can be a list of URLs or a JSON file from the scraper.')
    parser.add_argument('--downloadpath', type=str, metavar='P', default='./files', help='Path to download the tokens. Defaults to ./files')
    parser.add_argument('--listonly', action='store_true', help='List the token URLs without downloading.')
    parser.add_argument('--delay', type=float, metavar='S', default=5, help='Delay (in seconds) between page loads to prevent rate-limiting. Defaults to 5.')
    parser.add_argument('--timeout', type=float, metavar='S', default=10, help='Timeout (in seconds) when loading stuff. Defaults to 10.')
    parser.add_argument('--verbosity', type=str, choices=['error', 'warn', 'info', 'debug'], default='error', help='Desired logging level. Defaults to error.')
    args = parser.parse_args()

    logdict = {'error': logging.ERROR,
               'warn': logging.WARNING,
               'info': logging.INFO,
               'debug': logging.DEBUG}

    logging.basicConfig(level=logdict[args.verbosity], format='[%(levelname)s] (%(module)s) - %(message)s')
    logger = logging.getLogger('funge')
    suppresseddebugs = ['selenium', 'requests', 'urllib3', 'pyppeteer', 'websockets']
    for s in suppresseddebugs:
        l = logging.getLogger(s)
        l.setLevel(logging.INFO)

    main(args)