from requests_html import HTMLSession
import re
import argparse
import json
import math
from time import sleep
from pathlib import Path
import progressbar
import logging

def pull(session, continuation=None, size=100, sort='volume_desc'):
    sort = sort.upper().replace('-', '_')
    j = '{"filter":{"verifiedOnly":false,"sort":"'+sort+'","blockchains":["ETHEREUM","POLYGON","BASE","RARI","ASTARZKEVM","KROMA","ZKSYNC","IMMUTABLEX"],"showInRanking":false,"period":"DAY","hasCommunityMarketplace":false,"currency":"NATIVE"}}'
    J = json.loads(j)
    J['size'] = size
    if continuation:
        J['continuation'] = continuation

    url = 'https://rarible.com/marketplace/api/v4/collections/search'
    r = session.post(url, json=J)

    n = 0
    while n < 5 and 'X-CONTINUATION' not in r.headers.keys():
        logger.debug('Continuation not found, retrying.')
        sleep(1)
        r = session.post(url, json=J)
        n += 1
    if n == 5:
        logger.info('No continuation and out of retries; assuming this is the last pull.')
        continuation = None
        finished = True
    else:
        continuation = r.headers['X-CONTINUATION']
        logger.debug('Continuation is %s'%continuation)
        finished = False

    L = json.loads(r.text)

    collections = []
    for collection in L:
        id = collection['id']
        e = re.compile('([^\/]*-*)(\d+x[^\/]+)')
        g = e.search(id)

        linkbase = 'rarible.com/collection/'
        link = linkbase + '/'.join([g.group(1)[:-1].lower(), g.group(2)])
        link = 'https://' + link.replace('//', '/')

        name = collection['name']
        collections.append({'name': name, 'link': link})
        
    return collections, continuation, finished

def grabcollections(session, url, max, start=0, sort='volume_desc'):
    session.get(url)

    pullsize = min(1000, max)
    pulls = math.ceil(max / pullsize)

    pbar = progressbar.ProgressBar(max_value=pulls).start()
    collections = []
    continuation = None
    for i in range(pulls):
        pbar.update(i)
        collection, continuation, finished = pull(session, continuation, pullsize, sort=sort)
        collections += collection
        if finished:
            break
    pbar.finish()

    return collections[start:]

def zeropad(n, l):
    om = math.ceil(math.log10(l))
    s = str(n)
    L = om - len(s)
    return '0'*L+s

def main(args):
    downloadpath = args.downloadpath
    maxcollections = args.max
    start = args.start
    numcollections = maxcollections + start
    chunksize = args.chunksize if args.chunksize else (numcollections - start + 1)
    chunks = math.ceil((numcollections - start + 1) / chunksize)
    sort = re.compile('(sort=)(.*)$').search(args.params).group(2)
    
    s = HTMLSession()

    url = 'https://rarible.com/explore/all/collections?' + args.params
    collections = grabcollections(s, url, numcollections, start-1, sort=sort)
    for i in range(chunks):
        collection = collections[(i*chunksize):chunksize*(i+1)]
        JSON = json.dumps(collection)

        Path(downloadpath).mkdir(parents=True, exist_ok=True)
        with open(downloadpath+'/collections_'+zeropad(i, chunks)+'.json', "w") as outfile:
            outfile.write(JSON)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scrape some NFT collections from Rarible')
    parser.add_argument('--params', type=str, default='sort=volume-desc', help='Sort parameters to pass to Rarible. Defaults to sorting by volume, descdending.')
    parser.add_argument('--downloadpath', type=str, metavar='P', default='./', help='Path to download the tokens. Defaults to here')
    parser.add_argument('--max', type=int, default=1000, help='Maximum number of collections to scrape. Defaults to 1000.')
    parser.add_argument('--start', type=int, default=1, help='Position in the list to start scraping. Defaults to 1.')
    parser.add_argument('--chunksize', type=int, default=None, help='If specified, chunks the collections by the number specified. Off by default.')
    parser.add_argument('--verbosity', type=str, choices=['error', 'warn', 'info', 'debug'], default='error', help='Desired logging level. Defaults to error.')
    args = parser.parse_args()

    logger = logging.getLogger('collectionscrape')
    logdict = {'error': logging.ERROR,
                'warn': logging.WARNING,
                'info': logging.INFO,
                'debug': logging.DEBUG}
    logging.basicConfig(level=logdict[args.verbosity])

    main(args)